Pod::Spec.new do |s|
  s.name         = "Physics"
  s.version      = "0.0.1"
  s.summary      = "Physics Engine for Bubble Blast Saga"
  s.description  = <<-DESC
		Physics Engine for Bubble Blast Saga. ..
                   DESC

  s.homepage     = "https://www.gitbook.com/@cs3217"

  s.license      = { :type => 'MIT', :file => 'MITLICENSE.txt' }

  s.author             = { "Lim Kiang Teng" => "kiangtengl@outlook.com" }

  s.platform     = :ios, "10.0"

  s.source       = { :git => "git@bitbucket.org:cs3217/2017-ps4-a0122356m.git", :tag => "#{s.version}" }

  s.source_files  = "Physics", "BubbleBlastSaga/Physics/*.{swift}"

end
